minetest.register_tool("chemistry:pick_lava", {
	description = "Lava Pickaxe",
	inventory_image = "chemistry_tool_lava_pick.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=1,
		groupcaps={
			cracky={times={[1]=4.00, [2]=1.60, [3]=0.80}, uses=20, maxlevel=2},
		},
		damage_groups = {fleshy=6},
	},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool("chemistry:shovel_lava", {
	description = "Lava Shovel",
	inventory_image = "chemistry_tool_lava_shovel.png",
	wield_image = "chemistry_tool_lava_shovel.png^[transformR90",
	tool_capabilities = {
		full_punch_interval = 1.1,
		max_drop_level=1,
		groupcaps={
			crumbly = {times={[1]=1.50, [2]=0.90, [3]=0.40}, uses=30, maxlevel=2},
		},
		damage_groups = {fleshy=5},
	},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool("chemistry:axe_lava", {
	description = "Lava Axe",
	inventory_image = "chemistry_tool_lava_axe.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=1,
		groupcaps={
			choppy={times={[1]=2.50, [2]=1.40, [3]=1.00}, uses=20, maxlevel=2},
		},
		damage_groups = {fleshy=6},
	},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool("chemistry:sword_lava", {
	description = "Lava Sword",
	inventory_image = "chemistry_tool_lava_sword.png",
	tool_capabilities = {
		full_punch_interval = 0.8,
		max_drop_level=1,
		groupcaps={
			snappy={times={[1]=2.5, [2]=1.20, [3]=0.35}, uses=30, maxlevel=2},
		},
		damage_groups = {fleshy=8},
	},
	sound = {breaks = "default_tool_breaks"},
})

local function get_node_group(name, group)
    if not minetest.registered_nodes[name] or not minetest.registered_nodes[name].groups[group] then
        return 0
    end
    return minetest.registered_nodes[name].groups[group]
end

local old_handle_node_drops = minetest.handle_node_drops
function minetest.handle_node_drops(pos, drops, digger)
    local tool = digger:get_wielded_item():get_name()
    if tool == "chemistry:pick_lava" then
        local newdrops = { }
        for _, drop in ipairs(drops) do
            local stack = ItemStack(drop)
            if stack:get_name() == "default:cobble" then
                table.insert(newdrops, ItemStack("default:stone", stack:get_count()))
            elseif stack:get_name() == "default:mossycobble" then
                table.insert(newdrops, ItemStack("default:stone", stack:get_count()))
            elseif stack:get_name() == "default:iron_lump" then
                table.insert(newdrops, ItemStack("default:steel_ingot", stack:get_count()))
            elseif stack:get_name() == "default:copper_lump" then
                table.insert(newdrops, ItemStack("default:copper_ingot", stack:get_count()))
            elseif stack:get_name() == "default:tin_lump" then
                table.insert(newdrops, ItemStack("default:tin_ingot", stack:get_count()))
            elseif stack:get_name() == "default:gold_lump" then
                table.insert(newdrops, ItemStack("default:gold_ingot", stack:get_count()))
            elseif stack:get_name() == "chemistry:vanadium_lump" then
                table.insert(newdrops, ItemStack("chemistry:vanadium_ingot", stack:get_count()))
            elseif stack:get_name() == "default:obsidian" then
                table.insert(newdrops, ItemStack("default:obsidian_glass", stack:get_count()))
                table.insert(newdrops, ItemStack("default:obsidian_glass", stack:get_count()))
                table.insert(newdrops, ItemStack("default:obsidian_glass", stack:get_count()))
                table.insert(newdrops, ItemStack("default:obsidian_glass", stack:get_count()))
                table.insert(newdrops, ItemStack("default:obsidian_glass", stack:get_count()))
                table.insert(newdrops, ItemStack("default:obsidian_glass", stack:get_count()))
                table.insert(newdrops, ItemStack("default:obsidian_glass", stack:get_count()))
                table.insert(newdrops, ItemStack("default:obsidian_glass", stack:get_count()))
                table.insert(newdrops, ItemStack("default:obsidian_glass", stack:get_count()))
            elseif stack:get_name() == "default:desert_cobble" then
                table.insert(newdrops, ItemStack("default:desert_stone", stack:get_count()))
			elseif minetest.get_modpath("mesecons") ~= nil and stack:get_name() == "default:mese_crystal" then
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
				table.insert(newdrops, ItemStack("mesecons:mesecon_off", stack:get_count()))
            else
                table.insert(newdrops, stack)
            end
        end
        drops = newdrops
    elseif tool == "chemistry:shovel_lava" then
        local newdrops = { }
        for _, drop in ipairs(drops) do
            local stack = ItemStack(drop)
            if get_node_group(stack:get_name(), "sand") ~=0 then
                table.insert(newdrops, ItemStack("default:glass", stack:get_count()))
            else
                table.insert(newdrops, stack)
            end
        end
        drops = newdrops
    elseif tool == "chemistry:axe_lava" then
        local newdrops = { }
        for _, drop in ipairs(drops) do
            local stack = ItemStack(drop)
            if get_node_group(stack:get_name(), "tree") ~= 0 then
                table.insert(newdrops, ItemStack("chemistry:charcoal", stack:get_count()))
                table.insert(newdrops, ItemStack("chemistry:charcoal", stack:get_count()))
                table.insert(newdrops, ItemStack("chemistry:charcoal", stack:get_count()))
                table.insert(newdrops, ItemStack("chemistry:charcoal", stack:get_count()))
            elseif get_node_group(stack:get_name(), "wood") ~= 0 then
                table.insert(newdrops, ItemStack("chemistry:charcoal", stack:get_count()))
            else
                table.insert(newdrops, stack)
            end
        end
        drops = newdrops
    end
    return old_handle_node_drops(pos, drops, digger)
end

minetest.register_tool("chemistry:pick_stainless_steel", {
	description = "Stainless Steel Pickaxe",
	inventory_image = "chemistry_tool_stainless_pick.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=1,
		groupcaps={
			cracky = {times={[1]=4.00, [2]=1.60, [3]=0.80}, uses=40, maxlevel=2},
		},
		damage_groups = {fleshy=4},
	},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool("chemistry:shovel_stainless_steel", {
	description = "Stainless Steel Shovel",
	inventory_image = "chemistry_tool_stainless_shovel.png",
	wield_image = "chemistry_tool_stainless_shovel.png^[transformR90",
	tool_capabilities = {
		full_punch_interval = 1.1,
		max_drop_level=1,
		groupcaps={
			crumbly = {times={[1]=1.50, [2]=0.90, [3]=0.40}, uses=60, maxlevel=2},
		},
		damage_groups = {fleshy=3},
	},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool("chemistry:axe_stainless_steel", {
	description = "Stainless Steel Axe",
	inventory_image = "chemistry_tool_stainless_axe.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=1,
		groupcaps={
			choppy={times={[1]=2.50, [2]=1.40, [3]=1.00}, uses=40, maxlevel=2},
		},
		damage_groups = {fleshy=4},
	},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool("chemistry:sword_stainless_steel", {
	description = "Stainless Steel Sword",
	inventory_image = "chemistry_tool_stainless_sword.png",
	tool_capabilities = {
		full_punch_interval = 0.8,
		max_drop_level=1,
		groupcaps={
			snappy={times={[1]=2.5, [2]=1.20, [3]=0.35}, uses=60, maxlevel=2},
		},
		damage_groups = {fleshy=6},
	},
	sound = {breaks = "default_tool_breaks"},
})

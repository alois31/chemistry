minetest.register_node("chemistry:salt", {
    description = "Salt",
    tiles = {"chemistry_salt.png"},
    groups = {cracky = 3},
    sounds = default.node_sound_stone_defaults(),
    drop = 'chemistry:salt',
})

minetest.register_node("chemistry:stone_with_sulfur", {
    description = "Sulfur Ore",
    tiles = {"default_stone.png^chemistry_mineral_sulfur.png"},
    groups = {cracky = 3},
    drop = 'chemistry:sulfur_lump',
    sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("chemistry:sulfurized_mossy_cobble", {
    description = "Sulfurized Mossy Cobblestone",
    tiles = {"default_mossycobble.png^chemistry_mineral_sulfur.png"},
    groups = {cracky = 3},
    drop = 'chemistry:sulfurized_mossy_cobble',
    sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("chemistry:stone_with_vanadium", {
    description = "Vanadium Ore",
    tiles = {"default_stone.png^chemistry_mineral_vanadium.png"},
    groups = {cracky = 2},
    drop = 'chemistry:vanadium_lump',
    sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("chemistry:torch", {
	description = "Stinking Torch",
	drawtype = "torchlike",
	inventory_image = "chemistry_torch_floor.png",
	wield_image = "chemistry_torch_floor.png",
	tiles = {
        "chemistry_torch_floor.png",
        "chemistry_torch_ceiling.png",
        "chemistry_torch_wall.png",
    },
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	liquids_pointable = false,
	light_source = 13,
	groups = {choppy=2, dig_immediate=3, flammable=1, attached_node=1, torch=1},
	drop = "chemistry:torch",
	selection_box = {
		type = "wallmounted",
		wall_bottom = {-1/8, -1/2, -1/8, 1/8, 2/16, 1/8},
		wall_top = {-1/8, -1/16, -5/16, 1/8, 1/2, 1/8},
		wall_side = {-1/2, -1/2, -1/8, -1/8, 1/8, 1/8},
	},
	sounds = default.node_sound_wood_defaults(),
})

minetest.register_node("chemistry:magnesium_torch", {
	description = "Magnesium Torch",
	drawtype = "torchlike",
	inventory_image = "chemistry_magnesium_torch_floor.png",
	wield_image = "chemistry_magnesium_torch_floor.png",
	tiles = {
        "chemistry_magnesium_torch_floor.png",
        "chemistry_magnesium_torch_ceiling.png",
        "chemistry_magnesium_torch_wall.png",
    },
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	liquids_pointable = false,
	light_source = 14,
	groups = {choppy=2, dig_immediate=3, flammable=1, attached_node=1, torch=1},
	drop = "chemistry:magnesium_torch",
	selection_box = {
		type = "wallmounted",
		wall_bottom = {-1/8, -1/2, -1/8, 1/8, 2/16, 1/8},
		wall_top = {-1/8, -1/16, -5/16, 1/8, 1/2, 1/8},
		wall_side = {-1/2, -1/2, -1/8, -1/8, 1/8, 1/8},
	},
	sounds = default.node_sound_wood_defaults(),
})

minetest.register_node("chemistry:star", {
    description = "Star",
    tiles = {"chemistry_star.png"},
    drop = "chemistry:star",
    paramtype = "light",
    light_source = 14,
    sunlight_propagates = true,
    walkable = false,
    liquids_pointable = false,
    damage_per_second = 8,
    groups = {dig_immediate=2, igniter=1},
})

minetest.register_node("chemistry:helium_lamp", {
    description = "Helium Discharge Lamp",
    drawtype="signlike",
    inventory_image = "chemistry_helium_lamp.png",
    wield_image = "chemistry_helium_lamp.png",
    tiles = {"chemistry_helium_lamp.png"},
    paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	liquids_pointable = false,
	light_source = 13,
	groups = {choppy=2, dig_immediate=3, attached_node=1},
    drop = "chemistry:helium_lamp",
	selection_box = {
		type = "wallmounted",
	},
	sounds = default.node_sound_glass_defaults(),
})

minetest.register_node("chemistry:sodium_lamp", {
    description = "Sodium Discharge Lamp",
    drawtype="signlike",
    inventory_image = "chemistry_sodium_lamp.png",
    wield_image = "chemistry_sodium_lamp.png",
    tiles = {"chemistry_sodium_lamp.png"},
    paramtype = "light",
    paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	liquids_pointable = false,
	light_source = 13,
	groups = {choppy=2, dig_immediate=3, attached_node=1},
    drop = "chemistry:sodium_lamp",
	selection_box = {
        type = "wallmounted",
    },
	sounds = default.node_sound_glass_defaults(),
})

minetest.register_node("chemistry:hydrogen_lamp", {
    description = "Hydrogen Discharge Lamp",
    drawtype="signlike",
    inventory_image = "chemistry_hydrogen_lamp.png",
    wield_image = "chemistry_hydrogen_lamp.png",
    tiles = {"chemistry_hydrogen_lamp.png"},
    paramtype = "light",
    paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	liquids_pointable = false,
	light_source = 13,
	groups = {choppy=2, dig_immediate=3, attached_node=1},
    drop = "chemistry:hydrogen_lamp",
	selection_box = {
        type = "wallmounted",
    },
	sounds = default.node_sound_glass_defaults(),
})



minetest.register_node("chemistry:oxygen_lamp", {
    description = "Oxygen Discharge Lamp",
    drawtype="signlike",
    inventory_image = "chemistry_oxygen_lamp.png",
    wield_image = "chemistry_oxygen_lamp.png",
    tiles = {"chemistry_oxygen_lamp.png"},
    paramtype = "light",
    paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	liquids_pointable = false,
	light_source = 13,
	groups = {choppy=2, dig_immediate=3, attached_node=1},
    drop = "chemistry:oxygen_lamp",
	selection_box = {
        type = "wallmounted",
    },
	sounds = default.node_sound_glass_defaults(),
})



minetest.register_node("chemistry:arc_lamp", {
    description = "Arc Lamp",
    drawtype="signlike",
    inventory_image = "chemistry_arc_lamp.png",
    wield_image = "chemistry_arc_lamp.png",
    tiles = {"chemistry_arc_lamp.png"},
    paramtype = "light",
    paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	liquids_pointable = false,
	light_source = 13,
	groups = {choppy=2, dig_immediate=3, attached_node=1},
    drop = "chemistry:arc_lamp",
	selection_box = {
        type = "wallmounted",
    },
	sounds = default.node_sound_glass_defaults(),
})

minetest.register_node("chemistry:neon_lamp", {
    description = "Neon Discharge Lamp (Not Spain)",
    drawtype="signlike",
    inventory_image = "chemistry_neon_lamp.png",
    wield_image = "chemistry_neon_lamp.png",
    tiles = {"chemistry_neon_lamp.png"},
    paramtype = "light",
    paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	liquids_pointable = false,
	light_source = 13,
	groups = {choppy=2, dig_immediate=3, attached_node=1},
    drop = "chemistry:neon_lamp",
	selection_box = {
        type = "wallmounted",
    },
	sounds = default.node_sound_glass_defaults(),
})

minetest.register_node("chemistry:sulfur_lamp", {
    description = "Sulfur Lamp",
    drawtype="signlike",
    inventory_image = "chemistry_sulfur_lamp.png",
    wield_image = "chemistry_sulfur_lamp.png",
    tiles = {"chemistry_sulfur_lamp.png"},
    paramtype = "light",
    paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	liquids_pointable = false,
	light_source = 13,
	groups = {choppy=2, dig_immediate=3, attached_node=1},
    drop = "chemistry:sulfur_lamp",
	selection_box = {
        type = "wallmounted",
    },
	sounds = default.node_sound_glass_defaults(),
})

minetest.register_node("chemistry:compressed_coal", {
    description = "Compressed Coal",
    tiles = {"chemistry_compressed_coal.png"},
    is_ground_content=false,
    groups = {cracky=2},
    sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("chemistry:gypsum", {
    description = "Gypsum",
    tiles = {"chemistry_gypsum.png"},
    is_ground_content=false,
    groups = {cracky=3},
    sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("chemistry:copper_ii_sulfate_dry", {
    description = "Copper(II) Sulfate (Dry)",
    tiles = {"chemistry_copper_ii_sulfate_dry.png"},
    is_ground_content=false,
    groups = {crumbly=3, falling_node=1},
    sounds = default.node_sound_sand_defaults(),
})

minetest.register_node("chemistry:copper_ii_sulfate_wet", {
    description = "Copper(II) Sulfate (Wet)",
    tiles = {"chemistry_copper_ii_sulfate_wet.png"},
    is_ground_content=false,
    groups = {crumbly=3, falling_node=1},
    sounds = default.node_sound_sand_defaults(),
})

minetest.register_node("chemistry:star_radiation_zone_1", {
	description = "Radiation Zone (Gamma)",
	tiles = {"chemistry_star_atmosphere.png"},
	drop = "chemistry:hydrogen",
    paramtype = "light",
    light_source = 14,
    sunlight_propagates = true,
    walkable = false,
    liquids_pointable = false,
    damage_per_second = 8,
    groups = {dig_immediate=2, igniter=1},
})

minetest.register_node("chemistry:star_radiation_zone_2", {
	description = "Radiation Zone (X-Rays)",
	tiles = {"chemistry_star_atmosphere.png"},
	drop = "",
    paramtype = "light",
    light_source = 14,
    sunlight_propagates = true,
    walkable = false,
    liquids_pointable = false,
    damage_per_second = 8,
    groups = {dig_immediate=2, igniter=1},
})

minetest.register_node("chemistry:star_radiation_zone_3", {
	description = "Radiation Zone (Ultraviolet)",
	tiles = {"chemistry_star_atmosphere.png"},
	drop = "",
    paramtype = "light",
    light_source = 14,
    sunlight_propagates = true,
    walkable = false,
    liquids_pointable = false,
    damage_per_second = 8,
    groups = {dig_immediate=2, igniter=1},
})

minetest.register_node("chemistry:star_convection_zone", {
	description = "Convection Zone",
	tiles = {"chemistry_star_atmosphere.png"},
	drop = "",
    paramtype = "light",
    light_source = 14,
    sunlight_propagates = true,
    walkable = false,
    liquids_pointable = false,
    damage_per_second = 8,
    groups = {dig_immediate=2, igniter=1},
})



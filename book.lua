local pages = {}
local product_indexes = {}

local function representative(item_or_group_name)
    if item_or_group_name:sub(1, 6) == "group:" then
        local groupname = item_or_group_name:sub(7)
        -- TODO this works well for the current purposes, but isn't robust at all
        return "default:"..groupname
    else
        return item_or_group_name
    end
end

local function reactant_name_count(page, index)
    local reactants = reactions[page].reactants
    if reactants[index] then
        return representative(reactants[index]:get_name()).." "..reactants[index]:get_count()
    else
        return ""
    end
end

local function catalyst_name(page)
    local catalyst = reactions[page].catalyst
    if catalyst then
        return catalyst
    else
        return ""
    end
end

local function tool_name(page)
    local tool = reactions[page].tool
    if tool then
        return tool
    else
        return ""
    end
end

local function product_name_count(page, product_index, index)
    local output = reactions[page].possible_outputs[product_index]
    if output then
        if output.products[index] then
            return representative(output.products[index]:get_name()).." "..output.products[index]:get_count()
        else
            return ""
        end
    else
        return ""
    end
end

local function book_formspec(page, product_index)
    return
    "size[8,4]"..
    "label[1,0;Reactants "..page.."/"..#reactions.."]"..
    "label[5,0;Products "..product_index.."/"..#reactions[page].possible_outputs..": p="..reactions[page].possible_outputs[product_index].probability.."]"..
    "button[0,1;1,3;back;<]"..
    "button[7,1;1,3;forward;>]"..
    "item_image_button[1,1;1,1;"..reactant_name_count(page, 1)..";;]"..
    "item_image_button[2,1;1,1;"..reactant_name_count(page, 2)..";;]"..
    "item_image_button[1,2;1,1;"..reactant_name_count(page, 3)..";;]"..
    "item_image_button[2,2;1,1;"..reactant_name_count(page, 4)..";;]"..
    "item_image_button[1,3;1,1;"..reactant_name_count(page, 5)..";;]"..
    "item_image_button[2,3;1,1;"..reactant_name_count(page, 6)..";;]"..
    "item_image_button[3.5,1;1,1;"..catalyst_name()..";;]"..
    "item_image_button[3.5,3;1,1;"..tool_name()..";;]"..
    "item_image_button[5,1;1,1;"..product_name_count(page, product_index, 1)..";;]"..
    "item_image_button[6,1;1,1;"..product_name_count(page, product_index, 2)..";;]"..
    "item_image_button[5,2;1,1;"..product_name_count(page, product_index, 3)..";;]"..
    "item_image_button[6,2;1,1;"..product_name_count(page, product_index, 4)..";;]"..
    "item_image_button[5,3;1,1;"..product_name_count(page, product_index, 5)..";;]"..
    "item_image_button[6,3;1,1;"..product_name_count(page, product_index, 6)..";;]"
end

function book_on_use(itemstack, user)
    minetest.show_formspec(user:get_player_name(), "chemistry_book", book_formspec(1, 1))
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "chemistry_book" then
        return
    else
        local page = pages[player:get_player_name()]
        local product_index = product_indexes[player:get_player_name()]
        if fields.back then
            if product_index == 1 then
                page = page - 1
                product_index = #reactions[page].possible_outputs
            else
                product_index = product_index - 1
            end
        elseif fields.forward then
            if product_index == #reactions[page].possible_outputs then
                page = page + 1
                product_index = 1
            else
                product_index = product_index + 1
            end
        end
        if page == 0 then
            page = #reactions
        elseif page == #reactions + 1 then
            page = 1
        end
        pages[player:get_player_name()] = page
        product_indexes[player:get_player_name()] = product_index
        minetest.show_formspec(player:get_player_name(), "chemistry_book", book_formspec(page, product_index))
    end
end)

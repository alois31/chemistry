minetest.register_abm({
    nodenames = {"chemistry:copper_ii_sulfate_dry"},
    neighbors = {"default:water_source"},
    interval = 5.0,
    chance = 1,
    action = function(pos, node, active_object_count, active_object_count_wider)
        minetest.set_node(pos, {name = "chemistry:copper_ii_sulfate_wet"})
    end
})

minetest.register_abm({
    nodenames = {"chemistry:copper_ii_sulfate_dry"},
    neighbors = {"default:water_flowing"},
    interval = 5.0,
    chance = 4,
    action = function(pos, node, active_object_count, active_object_count_wider)
        minetest.set_node(pos, {name = "chemistry:copper_ii_sulfate_wet"})
    end
})

local function check_for_node(pos, name)
	local node = minetest.get_node(pos)
	return node.name == name
end

local function check_for_neighbor(pos, name)
	return check_for_node({x = pos.x - 1, y = pos.y - 1, z = pos.z - 1}, name)
		or check_for_node({x = pos.x - 1, y = pos.y - 1, z = pos.z}, name)
		or check_for_node({x = pos.x - 1, y = pos.y - 1, z = pos.z + 1}, name)
		or check_for_node({x = pos.x - 1, y = pos.y, z = pos.z - 1}, name)
		or check_for_node({x = pos.x - 1, y = pos.y, z = pos.z}, name)
		or check_for_node({x = pos.x - 1, y = pos.y, z = pos.z + 1}, name)
		or check_for_node({x = pos.x - 1, y = pos.y + 1, z = pos.z - 1}, name)
		or check_for_node({x = pos.x - 1, y = pos.y + 1, z = pos.z}, name)
		or check_for_node({x = pos.x - 1, y = pos.y + 1, z = pos.z + 1}, name)
		or check_for_node({x = pos.x, y = pos.y - 1, z = pos.z - 1}, name)
		or check_for_node({x = pos.x, y = pos.y - 1, z = pos.z}, name)
		or check_for_node({x = pos.x, y = pos.y - 1, z = pos.z + 1}, name)
		or check_for_node({x = pos.x, y = pos.y, z = pos.z - 1}, name)
		or check_for_node({x = pos.x, y = pos.y, z = pos.z + 1}, name)
		or check_for_node({x = pos.x, y = pos.y + 1, z = pos.z - 1}, name)
		or check_for_node({x = pos.x, y = pos.y + 1, z = pos.z}, name)
		or check_for_node({x = pos.x, y = pos.y + 1, z = pos.z + 1}, name)
		or check_for_node({x = pos.x + 1, y = pos.y - 1, z = pos.z - 1}, name)
		or check_for_node({x = pos.x + 1, y = pos.y - 1, z = pos.z}, name)
		or check_for_node({x = pos.x + 1, y = pos.y - 1, z = pos.z + 1}, name)
		or check_for_node({x = pos.x + 1, y = pos.y, z = pos.z - 1}, name)
		or check_for_node({x = pos.x + 1, y = pos.y, z = pos.z}, name)
		or check_for_node({x = pos.x + 1, y = pos.y, z = pos.z + 1}, name)
		or check_for_node({x = pos.x + 1, y = pos.y + 1, z = pos.z - 1}, name)
		or check_for_node({x = pos.x + 1, y = pos.y + 1, z = pos.z}, name)
		or check_for_node({x = pos.x + 1, y = pos.y + 1, z = pos.z + 1}, name)
end

minetest.register_abm({
    nodenames = {"chemistry:copper_ii_sulfate_wet"},
    interval = 5.0,
    chance = 8,
    action = function(pos, node, active_object_count, active_object_count_wider)
        if not (check_for_neighbor(pos, "default:water_source") or check_for_neighbor(pos, "default:water_flowing")) then
            minetest.set_node({x = pos.x, y = pos.y, z = pos.z}, {name = "chemistry:copper_ii_sulfate_dry"})
        end
    end
})

minetest.register_abm({
	nodenames = {"air", "chemistry:star_radiation_zone_1", "chemistry_star_radiation_zone_2", "chemistry:star_radiation_zone_3", "chemistry:star_convection_zone"},
	neighbors = {"chemistry:star"},
	interval = 2.0,
	chance = 10,
    action = function(pos, node, active_object_count, active_object_count_wider)
        minetest.set_node(pos, {name = "chemistry:star_radiation_zone_1"})
    end
})

minetest.register_abm({
	nodenames = {"chemistry:star_radiation_zone_1"},
	interval = 2.0,
	chance = 10,
	action = function(pos, node, active_object_count, active_object_count_wider)
		if not check_for_neighbor(pos, "chemistry:star") then
            minetest.set_node({x = pos.x, y = pos.y, z = pos.z}, {name = "air"})
        end
	end
})

minetest.register_abm({
	nodenames = {"air", "chemistry:star_radiation_zone_1", "chemistry_star_radiation_zone_2", "chemistry:star_radiation_zone_3", "chemistry:star_convection_zone"},
	neighbors = {"chemistry:star_radiation_zone_1"},
	interval = 2.0,
	chance = 20,
    action = function(pos, node, active_object_count, active_object_count_wider)
        minetest.set_node(pos, {name = "chemistry:star_radiation_zone_2"})
    end
})

minetest.register_abm({
	nodenames = {"chemistry:star_radiation_zone_2"},
	interval = 2.0,
	chance = 20,
	action = function(pos, node, active_object_count, active_object_count_wider)
		if not check_for_neighbor(pos, "chemistry:star_radiation_zone_1") then
            minetest.set_node({x = pos.x, y = pos.y, z = pos.z}, {name = "air"})
        end
	end
})

minetest.register_abm({
	nodenames = {"air", "chemistry:star_radiation_zone_1", "chemistry_star_radiation_zone_2", "chemistry:star_radiation_zone_3", "chemistry:star_convection_zone"},
	neighbors = {"chemistry:star_radiation_zone_2"},
	interval = 2.0,
	chance = 30,
    action = function(pos, node, active_object_count, active_object_count_wider)
        minetest.set_node(pos, {name = "chemistry:star_radiation_zone_3"})
    end
})

minetest.register_abm({
	nodenames = {"chemistry:star_radiation_zone_3"},
	interval = 2.0,
	chance = 30,
	action = function(pos, node, active_object_count, active_object_count_wider)
		if not check_for_neighbor(pos, "chemistry:star_radiation_zone_2") then
            minetest.set_node({x = pos.x, y = pos.y, z = pos.z}, {name = "air"})
        end
	end
})

minetest.register_abm({
	nodenames = {"air", "chemistry:star_radiation_zone_1", "chemistry_star_radiation_zone_2", "chemistry:star_radiation_zone_3", "chemistry:star_convection_zone"},
	neighbors = {"chemistry:star_radiation_zone_3"},
	interval = 2.0,
	chance = 40,
    action = function(pos, node, active_object_count, active_object_count_wider)
        minetest.set_node(pos, {name = "chemistry:star_convection_zone"})
    end
})

minetest.register_abm({
	nodenames = {"chemistry:star_convection_zone"},
	interval = 2.0,
	chance = 40,
	action = function(pos, node, active_object_count, active_object_count_wider)
		if not check_for_neighbor(pos, "chemistry:star_radiation_zone_3") then
            minetest.set_node({x = pos.x, y = pos.y, z = pos.z}, {name = "air"})
        end
	end
})

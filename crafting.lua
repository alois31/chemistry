minetest.register_craft({
	output = 'chemistry:torch 4',
	recipe = {
		{'chemistry:sulfur_lump'},
		{'group:stick'},
	}
})

minetest.register_craft({
	output = 'chemistry:magnesium_torch 4',
	recipe = {
		{'chemistry:magnesium'},
		{'group:stick'},
	}
})

minetest.register_craft({
    output = 'chemistry:pick_lava',
    recipe = {
        {'chemistry:refined_lava', 'chemistry:refined_lava', 'chemistry:refined_lava'},
        {'', 'group:stick', ''},
        {'', 'group:stick', ''},
    }
})

minetest.register_craft({
    output = 'chemistry:shovel_lava',
    recipe = {
        {'chemistry:refined_lava'},
        {'group:stick'},
        {'group:stick'},
    }
})

minetest.register_craft({
    output = 'chemistry:axe_lava',
    recipe = {
        {'chemistry:refined_lava', 'chemistry:refined_lava'},
        {'chemistry:refined_lava', 'group:stick'},
        {'', 'group:stick'},
    }
})

minetest.register_craft({
    output = 'chemistry:sword_lava',
    recipe = {
        {'chemistry:refined_lava'},
        {'chemistry:refined_lava'},
        {'group:stick'},
    }
})

minetest.register_craft({
    type = "shapeless",
    output = "chemistry:sulfurized_mossy_cobble",
    recipe = {"default:mossycobble", "chemistry:sulfur_lump"},
})

minetest.register_craft({
    type = 'cooking',
    output = 'chemistry:vanadium_ingot',
    recipe = 'chemistry:vanadium_lump',
})

minetest.register_craft({
	type = 'cooking',
	output = 'default:lava_source',
	recipe = 'chemistry:sulfurized_mossy_cobble',
	cooktime = 30,
})

minetest.register_craft({
	type = "fuel",
	recipe = "chemistry:sulfur_lump",
	burntime = 40,
})

minetest.register_craft({
    type = "shapeless",
    output = "chemistry:stainless_steel_ingot",
    recipe = {"default:steel_ingot", "chemistry:vanadium_ingot"},
})

minetest.register_craft({
    output = "chemistry:pick_stainless_steel",
    recipe = {
        {"chemistry:stainless_steel_ingot", "chemistry:stainless_steel_ingot", "chemistry:stainless_steel_ingot"},
        {"", "group:stick", ""},
        {"", "group:stick", ""},
    }
})

minetest.register_craft({
    output = "chemistry:shovel_stainless_steel",
    recipe = {
        {"chemistry:stainless_steel_ingot"},
        {"group:stick"},
        {"group:stick"},
    }
})

minetest.register_craft({
    output = "chemistry:axe_stainless_steel",
    recipe = {
        {"chemistry:stainless_steel_ingot", "chemistry:stainless_steel_ingot"},
        {"chemistry:stainless_steel_ingot", "group:stick"},
        {"", "group:stick"},
    }
})

minetest.register_craft({
    output = "chemistry:sword_stainless_steel",
    recipe = {
        {"chemistry:stainless_steel_ingot"},
        {"chemistry:stainless_steel_ingot"},
        {"group:stick"},
    }
})

minetest.register_craft({
    output = "chemistry:lab",
    recipe = {
        {"default:mese_crystal", "default:mese_crystal", "default:mese_crystal"},
        {"default:mese_crystal", "", "default:mese_crystal"},
        {"default:mese_crystal", "default:mese_crystal", "default:mese_crystal"},
    }
})

minetest.register_craft({
    output = "chemistry:bunsen",
    recipe = {
        {"group:torch"},
        {"default:steel_ingot"},
        {"default:steel_ingot"},
    }
})

minetest.register_craft({
    output = "chemistry:bucket_emptying_system",
    recipe = {
        {"bucket:bucket_empty"},
        {"default:steel_ingot"},
    }
})

minetest.register_craft({
    output = "chemistry:stir_stick 12",
    recipe = {
        {"default:glass"},
        {"default:glass"}
    }
})

minetest.register_craft({
    type = "cooking",
    output = "chemistry:sulfuric_acid",
    recipe = "chemistry:sulfur_trioxide_water",
    cooktime = 10,
})

minetest.register_craft({
    output = "chemistry:electrolytic_unit",
    recipe = {
        {"default:coal_lump", "", "default:coal_lump"},
        {"", "vessels:drinking_glass", ""},
    }
})

minetest.register_craft({
    output = "chemistry:gas_accumulation",
    recipe = {
        {"chemistry:hydrogen", "chemistry:hydrogen", "chemistry:hydrogen"},
        {"chemistry:hydrogen", "default:mese_crystal", "chemistry:hydrogen"},
        {"chemistry:hydrogen", "chemistry:hydrogen", "chemistry:hydrogen"},
    }
})

minetest.register_craft({
    type = "cooking",
    output = "chemistry:star",
    recipe = "chemistry:gas_accumulation",
    cooktime = 300,
})

minetest.register_craft({
    output = "chemistry:helium_lamp 12",
    recipe = {
        {"default:coal_lump", "chemistry:helium", "default:coal_lump"},
    }
})

minetest.register_craft({
    output = "chemistry:sodium_lamp 12",
    recipe = {
        {"default:coal_lump", "chemistry:sodium", "default:coal_lump"},
    }
})

minetest.register_craft({
    output = "chemistry:hydrogen_lamp 12",
    recipe = {
        {"default:coal_lump", "chemistry:hydrogen", "default:coal_lump"},
    }
})

minetest.register_craft({
    output = "chemistry:oxygen_lamp 12",
    recipe = {
        {"default:coal_lump", "chemistry:oxygen", "default:coal_lump"},
    }
})

minetest.register_craft({
    output = "chemistry:arc_lamp 12",
    recipe = {
        {"default:coal_lump", "", "default:coal_lump"},
    }
})

minetest.register_craft({
    output = "chemistry:neon_lamp 12",
    recipe = {
        {"default:coal_lump", "chemistry:neon", "default:coal_lump"},
    }
})

minetest.register_craft({
    output = "chemistry:sulfur_lamp 12",
    recipe = {
        {"default:coal_lump", "chemistry:sulfur_lump", "default:coal_lump"},
    }
})

minetest.register_craft({
    output = "chemistry:compressed_coal",
    recipe = {
        {"default:coalblock", "default:coalblock"},
        {"default:coalblock", "default:coalblock"},
    }
})

minetest.register_craft({
    type = "cooking",
    output = "default:diamond",
    recipe = "chemistry:compressed_coal",
    cooktime = 30,
})

minetest.register_craft({
    output = "chemistry:mortar",
    recipe = {
        {"", "", "default:clay_brick"},
        {"default:clay_brick", "", "default:clay_brick"},
        {"", "default:clay_brick", ""},
    }
})

minetest.register_craft({
	output = "default:coal_lump",
	recipe = {
		{"chemistry:charcoal", "chemistry:charcoal", "chemistry:charcoal"},
		{"chemistry:charcoal", "chemistry:charcoal", "chemistry:charcoal"},
		{"chemistry:charcoal", "chemistry:charcoal", "chemistry:charcoal"},
	}
})

minetest.register_craft({
	type = "shapeless",
	output = "chemistry:refined_lava",
	recipe = {"default:lava_source", "default:steel_ingot"}
})

minetest.register_craft({
    type = "shapeless",
    output = "chemistry:book",
    recipe = {"default:book", "chemistry:sulfur_lump"}
})

-- Mesecon compatibility/support
if minetest.get_modpath("mesecons") ~= nil then
	minetest.register_craft({
		type = "shapeless",
		output = "mesecons_materials:silicon",
		recipe = {"chemistry:silicon"},
	})
	minetest.register_craft({
		type = "shapeless",
		output = "chemistry:silicon",
		recipe = {"mesecons_materials:silicon"},
	})
end

register_reaction({
    tool = "chemistry:bunsen",
    reactants = {ItemStack("chemistry:vanadium_ingot 2")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:vanadium_v_oxide 1")}
        }
    },
    sound = "bunsen"
})

register_reaction({
    tool = "chemistry:bunsen",
    reactants = {ItemStack("chemistry:sulfur_lump 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:sulfur_dioxide 1")}
        }
    },
    sound = "bunsen"
})

register_reaction({
    tool = "chemistry:bunsen",
    catalyst = "chemistry:vanadium_v_oxide",
    reactants = {ItemStack("chemistry:sulfur_dioxide 2")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:sulfur_trioxide 2")}
        }
    },
    sound = "bunsen"
})

register_reaction({
    tool = "chemistry:bucket_emptying_system",
    reactants = {ItemStack("bucket:bucket_water 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("default:water_source 1"), ItemStack("bucket:bucket_empty 1")}
        }
    },
    sound = "pouring"
})

register_reaction({
    tool = "chemistry:bucket_emptying_system",
    reactants = {ItemStack("bucket:bucket_lava 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("default:lava_source 1"), ItemStack("bucket:bucket_empty 1")}
        }
    },
    sound = "pouring"
})

register_reaction({
    tool = "chemistry:electrolytic_unit",
    reactants = {ItemStack("default:water_source 2")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:hydrogen 2"), ItemStack("chemistry:oxygen 1")}
        }
    }
})

register_reaction({
    tool = "chemistry:electrolytic_unit",
    reactants = {ItemStack("chemistry:salt 2")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:sodium 2"), ItemStack("chemistry:chlorine 1")}
        }
    }
})

register_reaction({
    tool = "chemistry:star",
    reactants = {ItemStack("chemistry:hydrogen 4")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:helium 1")}
        }
    }
})

register_reaction({
    tool = "chemistry:star",
    reactants = {ItemStack("chemistry:helium 3")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("default:coal_lump 1")}
        }
    }
})

register_reaction({
    tool = "chemistry:star",
    reactants = {ItemStack("default:coal_lump 2")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:neon 1"), ItemStack("chemistry:helium 1")}
        }
    }
})

register_reaction({
    tool = "chemistry:star",
    reactants = {ItemStack("chemistry:oxygen 2")},
    possible_outputs = {
        {
            probability = 0.5,
            products = {ItemStack("chemistry:sulfur_lump 1")}
        },
        {
            probability = 0.5,
            products = {ItemStack("chemistry:silicon 1"), ItemStack("chemistry:helium 1")}
        }
    }
})

register_reaction({
    tool = "chemistry:star",
    reactants = {ItemStack("chemistry:silicon 2")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("default:iron_lump 1")}
        }
    }
})

register_reaction({
    tool = "chemistry:bunsen",
    reactants = {ItemStack("chemistry:silicon 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("default:sand 1")}
        }
    },
    sound = "bunsen"
})

register_reaction({
    tool = "chemistry:mortar",
    reactants = {ItemStack("default:cobble 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("default:gravel 1")}
        }
    },
    sound = "mortar"
})

register_reaction({
    tool = "chemistry:mortar",
    reactants = {ItemStack("default:gravel 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("default:sand 1")}
        }
    },
    sound = "mortar"
})

register_reaction({
    tool = "chemistry:mortar",
    reactants = {ItemStack("default:sand 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("default:clay 1")}
        }
    },
    sound = "mortar"
})

register_reaction({
    tool = "chemistry:mortar",
    reactants = {ItemStack("group:tree 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("default:dirt 8")}
        }
    },
    sound = "mortar"
})

register_reaction({
    tool = "chemistry:mortar",
    reactants = {ItemStack("group:wood 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("default:dirt 2")}
        }
    },
    sound = "mortar"
})

register_reaction({
    tool = "chemistry:bunsen",
    reactants = {ItemStack("group:tree 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:charcoal 4")}
        }
    },
    sound = "bunsen"
})

register_reaction({
    tool = "chemistry:bunsen",
    reactants = {ItemStack("group:wood 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:charcoal 1")}
        }
    },
    sound = "bunsen"
})

register_reaction({
    tool = "chemistry:star",
    reactants = {ItemStack("default:iron_lump 1")},
    possible_outputs = { -- The probabilities are approximately proportional to the occurence at very low y
        {
            probability = 0.351,
            products = {ItemStack("default:iron_lump 1")}
        },
        {
            probability = 0.309,
            products = {ItemStack("chemistry:vanadium_lump 1")}
        },
        {
            probability = 0.146,
            products = {ItemStack("default:copper_lump 1")}
        },
        {
            probability = 0.106,
            products = {ItemStack("default:tin_lump 1")}
        },
        {
            probability = 0.048,
            products = {ItemStack("default:gold_lump 1")}
        },
        {
            probability = 0.039,
            products = {ItemStack("default:mese_crystal 1")}
        },
        {
            probability = 0.001,
            products = {ItemStack("default:mese 1")}
        }
    }
})

register_reaction({
    tool = "chemistry:bunsen",
    reactants = {ItemStack("chemistry:copper_ii_sulfate_wet 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:copper_ii_sulfate_dry 1")}
        }
    },
    sound = "bunsen"
})

register_reaction({
    tool = "chemistry:bunsen",
    reactants = {ItemStack("default:water_source 10")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:salt 1")}
        }
    },
    sound = "bunsen"
})

register_reaction({
    tool = "chemistry:bunsen",
    reactants = {ItemStack("chemistry:hydrogen 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("default:water_source 1")}
        }
    },
    sound = "explosion"
})

register_reaction({
    tool = "chemistry:stir_stick",
    reactants = {ItemStack("chemistry:sulfur_trioxide 1"), ItemStack("default:water_source 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:sulfur_trioxide_water 1")}
        }
    },
    sound = "stirring"
})

register_reaction({
    tool = "chemistry:stir_stick",
    reactants = {ItemStack("chemistry:sulfur_trioxide 1"), ItemStack("chemistry:sulfuric_acid 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:disulfuric_acid 1")}
        }
    },
    sound = "stirring"
})

register_reaction({
    tool = "chemistry:stir_stick",
    reactants = {ItemStack("chemistry:disulfuric_acid 1"), ItemStack("default:water_source 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:sulfuric_acid 2")}
        }
    },
    sound = "stirring"
})

register_reaction({
    tool = "chemistry:bunsen",
    reactants = {ItemStack("chemistry:hydrogen 1"), ItemStack("chemistry:chlorine 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:hydrogen_chloride 2")}
        }
    },
    sound = "explosion"
})

register_reaction({
    tool = "chemistry:stir_stick",
    reactants = {ItemStack("chemistry:hydrogen_chloride 1"), ItemStack("default:water_source 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:hydrochloric_acid 1")}
        }
    },
    sound = "stirring"
})

register_reaction({
    tool = "chemistry:stir_stick",
    reactants = {ItemStack("chemistry:sodium 2"), ItemStack("default:water_source 2")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:sodium_hydroxide 2"), ItemStack("chemistry:hydrogen 1")}
        }
    },
    sound = "stirring"
})

register_reaction({
    tool = "chemistry:stir_stick",
    reactants = {ItemStack("chemistry:sodium_hydroxide 1"), ItemStack("default:water_source 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:caustic_soda 1")}
        }
    },
    sound = "stirring"
})

register_reaction({
    tool = "chemistry:star",
    reactants = {ItemStack("default:coal_lump 1"), ItemStack("chemistry:helium 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:oxygen 1")}
        }
    }
})

register_reaction({
    tool = "chemistry:star",
    reactants = {ItemStack("chemistry:neon 1"), ItemStack("chemistry:helium 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:magnesium 1")}
        }
    }
})

register_reaction({
    tool = "chemistry:star",
    reactants = {ItemStack("chemistry:oxygen 1"), ItemStack("chemistry:helium 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:neon 1")}
        }
    }
})

register_reaction({
    tool = "chemistry:stir_stick",
    catalyst = "chemistry:bunsen",
    reactants = {ItemStack("default:copper_lump 1"), ItemStack("chemistry:sulfuric_acid 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:copper_ii_sulfate_dry 1"), ItemStack("chemistry:hydrogen 1")}
        }
    },
    sound = "bunsen"
})

register_reaction({
    tool = "chemistry:stir_stick",
    catalyst = "chemistry:bunsen",
    reactants = {ItemStack("default:copper_ingot 1"), ItemStack("chemistry:sulfuric_acid 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:copper_ii_sulfate_dry 1"), ItemStack("chemistry:hydrogen 1")}
        }
    },
    sound = "bunsen"
})

register_reaction({
    tool = "chemistry:stir_stick",
    reactants = {ItemStack("group:stone 1"), ItemStack("chemistry:sulfuric_acid 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:gypsum 1")}
        }
    },
    sound = "stirring"
})

register_reaction({
    tool = "chemistry:stir_stick",
    reactants = {ItemStack("default:gravel 1"), ItemStack("chemistry:sulfuric_acid 1")},
    possible_outputs = {
        {
            probability = 1.0,
            products = {ItemStack("chemistry:gypsum 1")}
        }
    },
    sound = "stirring"
})


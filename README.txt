Adds ores, a lab, various lab equipment, chemicals and more tools, building materials and lights. Very comprehensive chemistry mod for Minetest.

License: LGPLv2.1 or later for code, CC-BY-SA 3.0 for textures and sounds

Version: 0.2

Installation:
GNU/Linux with system-wide installation: copy the mod folder to ~/.minetest/mods/
Other: copy the mod folder to minetest/mods/ in your installation folder

Dependencies: Nothing outside of the Minetest game, so everything needed should be already in place. We need only default, vessels, bucket and fire.
Mesecons will only add some support/compatibility features.
Minetest 0.4.17.1 is recommended.

Contact: alois1@gmx-topmail.de


Mod Contents
============

Ores
----
* Sulfur: Used for stinking torches and as a starting point for sulfuric acid
* Vanadium: Used to make a catalyst and stainless steel (lump smelts to ingot)
* Salt: Used to make sodium and chlorine

Lab Equipment
-------------
* Bunsen Burner: Used to provide heat for various reactions
* Stir Stick: Used to mix reactants
* Bucket Emptying System: Does what it says :)
* Electrolytic Unit: Splits water and salt into the elements
* Star: Used for various nuclear reactions
* Mortar: Used to make things smaller

Chemicals
---------
GASES
* Hydrogen: Used for lamps and to make hydrochloric acid, starting point for nuclear reactions, burn it to make water
* Helium: Used for lamps and in nuclear reactions
* Oxygen: Used for lamps and in nuclear reactions
* Neon: Used for lamps and in nuclear reactions
* Sulfur Dioxide: Make sulfur trioxide
* Chlorine: Make hydrogen chloride
* Hydrogen Chloride: Make hydrochloric acid
METALS
* Sodium: Used for lamps and for sodium hydroxide
* Magnesium: Used to make magnesium torches
* Silicon: Used in nuclear reactions and to make sand
OTHER
* Sulfur Trioxide: Make sulfuric acid
* Vanadium(V) Oxide: Catalyst for creating sulfur trioxide
* Sulfur Trioxide With Water: Cook it to make sulfuric acid
* Sodium Hydroxide: Make caustic soda
BASES
* Caustic Soda: Currently useless
ACIDS
* Sulfuric Acid: Make copper sulfate and gypsum, make disulfuric acid
* Disulfuric Acid: Make sulfuric acid faster than with cooking
* Hydrochloric Acid: Currently useless

Misc
----
* Stainless Steel Ingot: Make tools
* Collection Of Interstellar Gas: Cook it for five minutes to start a star
* Charcoal: Intermediate product to make coal from trees
* Compressed Coal: Make diamond renewable
* Refined lava: Make lava tools
* Old Alchemy Textbook: Contains all possible reactions (in-game documentation)

Tools
-----
* Lava pickaxe, shovel, axe and sword: Lava tools are like steel tools, but smelt most dropped items (and turn wood into charcoal), also the sword deals more damage
* Stainless Steel pickaxe, shovel, axe and sword: More robust than steel tools

Blocks
------
* Copper Sulfate: Indicates whether there is water nearby
* Gypsum: Used as a building material
* Sulfurized Mossy Cobblestone: Cook it to make lava

Lights
------
* Stinking Torch: Torch with sulfur instead of coal
* Magnesium Torch: Torch with magnesium instead of coal (or sulfur), slightly brighter
* Arc Lamp
* Discharge Lamps: Hydrogen, Helium, Oxygen, Neon, Sodium, Sulfur

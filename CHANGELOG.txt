0.3:
* Extensive code refactoring to prepare for an API (which is already used internally, but unstable and not exposed at the moment)
* Added in-game documentation ("Old Alchemy Textbook")
* Made vanadium more common to better reflect the situation in real life

0.2:
* Water can now be crafted by burning hydrogen
* Updated README to reflect changes in the uses for some items
* Salt is now less common, it's mostly useless anyway
* Added small chemical symbols to most chemicals to make them more distinguishable
* Made coal less easily obtainable from wood
* Added magnesium torch
* Mesecon compatibility/support
* Added sounds

0.1: Initial testing version

-- No API is exported yet, as it's unstable
chemistry = {}

local chemistry_path=minetest.get_modpath("chemistry")

--[[
    Dependencies:
        reactions > lab
        book > lab
        craftitems > book
]]--
dofile(chemistry_path.."/nodes.lua")
dofile(chemistry_path.."/ores.lua")
dofile(chemistry_path.."/tools.lua")
dofile(chemistry_path.."/lab.lua")
dofile(chemistry_path.."/reactions.lua")
dofile(chemistry_path.."/crafting.lua")
dofile(chemistry_path.."/abm.lua")
dofile(chemistry_path.."/book.lua")
dofile(chemistry_path.."/craftitems.lua")

minetest.register_craftitem("chemistry:sulfur_lump", {
    description = "Sulfur Lump",
    inventory_image = "chemistry_sulfur_lump.png",
})

minetest.register_craftitem("chemistry:vanadium_lump", {
    description = "Vanadium Lump",
    inventory_image = "chemistry_vanadium_lump.png",
})

minetest.register_craftitem("chemistry:vanadium_ingot", {
    description = "Vanadium Ingot",
    inventory_image = "chemistry_vanadium_ingot.png",
})

minetest.register_craftitem("chemistry:stainless_steel_ingot", {
    description = "Stainless Steel Ingot",
    inventory_image = "chemistry_stainless_steel_ingot.png",
})

minetest.register_craftitem("chemistry:bunsen", {
    description = "Bunsen Burner",
    inventory_image = "chemistry_bunsen.png",
})

minetest.register_craftitem("chemistry:vanadium_v_oxide", {
    description = "Vanadium(V) Oxide",
    inventory_image = "chemistry_vanadium_v_oxide.png",
})

minetest.register_craftitem("chemistry:sulfur_dioxide", {
    description = "Sulfur Dioxide",
    inventory_image = "chemistry_sulfur_dioxide.png",
})

minetest.register_craftitem("chemistry:sulfur_trioxide", {
    description = "Sulfur Trioxide",
    inventory_image = "chemistry_sulfur_trioxide.png",
})

minetest.register_craftitem("chemistry:bucket_emptying_system", {
    description = "Bucket Emptying System",
    inventory_image = "chemistry_bucket_emptying.png",
})

minetest.register_craftitem("chemistry:sulfur_trioxide_water", {
    description = "Sulfur Trioxide With Water",
    inventory_image = "chemistry_sulfur_trioxide_water.png",
})

minetest.register_craftitem("chemistry:sulfuric_acid", {
    description = "Sulfuric Acid",
    inventory_image = "chemistry_sulfuric_acid.png",
})

minetest.register_craftitem("chemistry:stir_stick", {
    description = "Stir Stick",
    inventory_image = "chemistry_stir_stick.png",
})

minetest.register_craftitem("chemistry:disulfuric_acid", {
    description = "Disulfuric Acid",
    inventory_image = "chemistry_disulfuric_acid.png",
})

minetest.register_craftitem("chemistry:electrolytic_unit", {
    description = "Electrolytic Unit",
    inventory_image = "chemistry_electrolytic_unit.png",
})

minetest.register_craftitem("chemistry:hydrogen", {
    description = "Hydrogen",
    inventory_image = "chemistry_hydrogen.png",
})

minetest.register_craftitem("chemistry:sodium", {
    description = "Sodium",
    inventory_image = "chemistry_sodium.png",
})

minetest.register_craftitem("chemistry:chlorine", {
    description = "Chlorine",
    inventory_image = "chemistry_chlorine.png",
})

minetest.register_craftitem("chemistry:sodium_hydroxide", {
    description = "Sodium Hydroxide",
    inventory_image="chemistry_sodium_hydroxide.png",
})

minetest.register_craftitem("chemistry:caustic_soda", {
    description = "Caustic Soda",
    inventory_image = "chemistry_caustic_soda.png",
})

minetest.register_craftitem("chemistry:hydrogen_chloride", {
    description = "Hydrogen Chloride",
    inventory_image = "chemistry_hydrogen_chloride.png",
})

minetest.register_craftitem("chemistry:hydrochloric_acid", {
    description = "Hydrochloric Acid",
    inventory_image = "chemistry_hydrochloric_acid.png",
})

minetest.register_craftitem("chemistry:gas_accumulation", {
    description = "Accumulation Of Interstellar Gas",
    inventory_image = "chemistry_gas_accumulation.png",
})

minetest.register_craftitem("chemistry:helium", {
    description = "Helium",
    inventory_image = "chemistry_helium.png",
})

minetest.register_craftitem("chemistry:oxygen", {
    description = "Oxygen",
    inventory_image = "chemistry_oxygen.png",
})

minetest.register_craftitem("chemistry:neon", {
    description = "Neon",
    inventory_image = "chemistry_neon.png",
})

minetest.register_craftitem("chemistry:magnesium", {
    description = "Magnesium",
    inventory_image = "chemistry_magnesium.png",
})

minetest.register_craftitem("chemistry:silicon", {
    description = "Silicon",
    inventory_image = "chemistry_silicon.png",
})

minetest.register_craftitem("chemistry:mortar", {
    description = "Mortar",
    inventory_image = "chemistry_mortar.png",
})

minetest.register_craftitem("chemistry:charcoal", {
	description = "Charcoal",
	inventory_image = "chemistry_charcoal.png",
})

minetest.register_craftitem("chemistry:refined_lava", {
	description = "Refined Lava",
	inventory_image = "chemistry_refined_lava.png",
})

minetest.register_craftitem("chemistry:book", {
    description = "Old Alchemy Textbook",
    inventory_image = "chemistry_book.png",
    stack_max = 1,
    groups = {book = 1},
    on_use = function(itemstack, user)
        book_on_use(itemstack, user)
    end,
})
